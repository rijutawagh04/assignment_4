/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package department;

import java.util.ArrayList;

/**
 *
 * @author kruts
 */
public class DepartmentDirectory {
    
    private ArrayList<Department> departmentDirectory;
    Department department1;
    Department department2;
    Department department3;
    
    public DepartmentDirectory(){
        departmentDirectory = new ArrayList<>();
        department1 = new Department();
        department1.setCollege("College of Engineering");
        department1.setDeptName("Information Systems");
        departmentDirectory.add(department1);
    
        
         
        department2 = new Department();
        department2.setCollege("College of Engineering");
        department2.setDeptName("Computer Science");
        departmentDirectory.add(department2);
        
        department3 = new Department();
        department3.setCollege("Colleg of Engineering");
        department3.setDeptName("Industrial Engineering");
        departmentDirectory.add(department3);
        
    }

    public Department getDepartment1() {
        return department1;
    }

    public void setDepartment1(Department department1) {
        this.department1 = department1;
    }

    public Department getDepartment2() {
        return department2;
    }

    public void setDepartment2(Department department2) {
        this.department2 = department2;
    }

    public Department getDepartment3() {
        return department3;
    }

    public void setDepartment3(Department department3) {
        this.department3 = department3;
    }
    
    
    
}
