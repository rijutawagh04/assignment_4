/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package department;

/**
 *
 * @author kruts
 */

public class Department {
    private String deptName;
    private String deptDirector;
    private String deptID;
    private String college;
    private String studyOptions;
    
    
    public Department()
    {
 
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public void setDeptDirector(String deptDirector) {
        this.deptDirector = deptDirector;
    }

    public void setDeptID(String deptID) {
        this.deptID = deptID;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public void setStudyOptions(String studyOptions) {
        this.studyOptions = studyOptions;
    }

    public String getDeptName() {
        return deptName;
    }

    public String getDeptDirector() {
        return deptDirector;
    }

    public String getDeptID() {
        return deptID;
    }

    public String getCollege() {
        return college;
    }

    public String getStudyOptions() {
        return studyOptions;
    }
    
    

        
      
     
}
